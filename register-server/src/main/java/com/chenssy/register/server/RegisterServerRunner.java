package com.chenssy.register.server;

import com.chenssy.register.core.ServiceStatusMonitor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RegisterServerRunner implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("开启服务状态监控器...");
        ServiceStatusMonitor monitor = new ServiceStatusMonitor();
        monitor.start();
    }
}
