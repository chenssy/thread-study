package com.chenssy.register.core;

import lombok.Data;

/**
 * 自我保护机制
 * @author:chenssy
 * @date:2021/7/14
 */
@Data
public class SelfProtector {

    private SelfProtector(){

    }

    private static class SelfProtectorHolder {
        private static SelfProtector instance = new SelfProtector();
    }

    public static SelfProtector getInstance() {
        return SelfProtectorHolder.instance;
    }



    private Long expectedHeartbeatRate;

    private Long expectedHearbeatThreshold;

    /**
     *
     * @return
     */
    public Boolean isSelfProtection() {
        HearbeatMessureRate messureRate = HearbeatMessureRate.getInstance();

        Long lastMinuteHearbeatRate = messureRate.getLastMinuteHearbeatRate();
        if (lastMinuteHearbeatRate < this.getExpectedHearbeatThreshold()) {
            return  true;
        }

        return false;
    }
}
