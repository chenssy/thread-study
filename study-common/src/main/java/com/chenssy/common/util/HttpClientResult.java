package com.chenssy.common.util;

import lombok.Data;

import java.io.Serializable;

@Data
public class HttpClientResult implements Serializable {
    /**
     * 响应状态码
     */
    private int code;

    /**
     * 响应数据
     */
    private String content;

}
