package com.chenssy.common.core;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 最近变化的服务实例
 * @author:chenssy
 * @date:2021/7/21
 */
@Data
@AllArgsConstructor
public class RecentlyChangedServiceInstance {

    /**
     * 服务实例
     */
    private InstanceInfo instanceInfo;

    /**
     * 更新时间
     */
    private Long changedTimeStamp;

    /**
     * 操作类型
     */
    private String changedOperation;
}
