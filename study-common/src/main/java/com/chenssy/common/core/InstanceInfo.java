package com.chenssy.common.core;

import lombok.Builder;
import lombok.Data;

/**
 * 服务实例
 * @author:chenssy
 * @date:2021/6/30
 */
@Data
@Builder
public class InstanceInfo {


    /** 服务实例 ID */
    private String serviceInstanceId;

    /** 服务名称 */
    private String serviceName;

    /** IP */
    private String ip;

    /** 主机名 */
    private String hostName;

    /** 端口 */
    private Integer port;

}
