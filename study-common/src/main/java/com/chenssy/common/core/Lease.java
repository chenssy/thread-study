package com.chenssy.common.core;

import lombok.Data;

/**
 * 契约
 * @author:chenssy
 * @date:2021/6/30
 */
@Data
public class Lease<T> {

    private T holder;

    /**
     * 注册时间
     */
    private Long registrationTimestamp;

    /**
     * 最后更新时间
     */
    private Long lastUpdateTimestamp;

    /**
     * 续约时间
     */
    private Long duration;

    public Lease(T holder) {
        this.holder = holder;
        this.registrationTimestamp = System.currentTimeMillis();
        this.duration = 30 * 1000L;
        this.lastUpdateTimestamp = registrationTimestamp + duration;
    }


    /**
     * 服务续约
     */
    public void renew() {
        lastUpdateTimestamp = System.currentTimeMillis() + duration;
    }
}
