package com.chenssy.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum  ResponseStatusEnum {

    SUCCESS("200","成功"),
    ERROR("201","请求失败");
    ;

    private String code;

    private String message;
}
