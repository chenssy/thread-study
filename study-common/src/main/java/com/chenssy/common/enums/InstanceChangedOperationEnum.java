package com.chenssy.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum  InstanceChangedOperationEnum {

    register("register","注册"),
    remove("remove","删除"),
    ;
    private String operation;

    private String message;
}
