package com.chenssy.common.dto;

import com.chenssy.common.enums.ResponseStatusEnum;
import lombok.Data;

@Data
public class HearbeatResponse {

    private String status;

    public HearbeatResponse() {

    }

    public HearbeatResponse(ResponseStatusEnum responseStatusEnum) {
        this.status = responseStatusEnum.getCode();
    }
}
