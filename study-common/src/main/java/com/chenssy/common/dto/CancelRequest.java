package com.chenssy.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CancelRequest {

    /**
     * 服务名称
     */
    private String serviceName;

    /** 服务实例 ID */
    private String serviceInstanceId;
}
