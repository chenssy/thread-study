package com.chenssy.common.dto;

import lombok.Data;

@Data
public class HearbeatRequest {

    /**
     * 服务名称
     */
    private String serviceName;

    /** 服务实例 ID */
    private String serviceInstanceId;
}
