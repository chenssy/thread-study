package com.chenssy.common.dto;

import com.chenssy.common.enums.ResponseStatusEnum;
import lombok.Data;

@Data
public class CancelResponse {
    private String code;

    private String message;

    public CancelResponse() {

    }

    public CancelResponse(ResponseStatusEnum response) {
        this.code = response.getCode();
        this.message = response.getMessage();
    }
}
