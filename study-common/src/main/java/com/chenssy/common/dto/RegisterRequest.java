package com.chenssy.common.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegisterRequest {

    /** 服务实例 ID */
    private String serviceInstanceId;

    /** 服务名称 */
    private String serviceName;

    /** IP */
    private String ip;

    /** 主机名 */
    private String hostName;

    /** 端口 */
    private Integer port;
}
