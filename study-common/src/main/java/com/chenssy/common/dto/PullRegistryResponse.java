package com.chenssy.common.dto;

import com.chenssy.common.core.InstanceInfo;
import com.chenssy.common.core.Lease;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class PullRegistryResponse {

    private Map<String, Map<String, Lease<InstanceInfo>>> registry = new HashMap<>();
}
