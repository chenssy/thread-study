package com.chenssy.common.dto;

import com.chenssy.common.enums.ResponseStatusEnum;
import lombok.Data;

@Data
public class RegisterResponse {

    private String code;

    private String message;

    public RegisterResponse() {

    }

    public RegisterResponse(ResponseStatusEnum response) {
        this.code = response.getCode();
        this.message = response.getMessage();
    }
}
