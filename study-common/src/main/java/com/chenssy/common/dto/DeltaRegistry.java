package com.chenssy.common.dto;

import com.chenssy.common.core.RecentlyChangedServiceInstance;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class DeltaRegistry {
    private List<RecentlyChangedServiceInstance> deltaRegistryList;

    private Long registryTotal;
}
