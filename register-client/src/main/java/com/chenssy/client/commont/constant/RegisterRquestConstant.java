package com.chenssy.client.commont.constant;

/**
 * 服务端请求地址
 * @author:chenssy
 * @date:2021/7/5
 */
public class RegisterRquestConstant {

    /**
     * 注册地址
     */
    public final static String REGISTER_REQUEST = "http://localhost:8089/register";

    /**
     * 续约
     */
    public final static String RENEW_REQUEST = "http://localhost:8089/renew";

    /**
     * 拉取注册表
     */
    public final static String PULL_REGISTRY_REQEUST = "http://localhost:8089/pullRegistry";

    public final static String delta_pull_REGISTRY_REQEUST = "http://localhost:8089/pullRegistry";


    /**
     * 服务下线
     */
    public final static String CANCEL_REQUEST = "http://localhost:8089/cancel";
}
