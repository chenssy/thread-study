package com.chenssy.client;

import com.chenssy.client.core.RegisterClient;

import java.util.UUID;

public class RegisterClientTest {

    private static final String SERVICE_INSTANCE_ID = UUID.randomUUID().toString().replaceAll("-","");

    public static void main(String[] args) throws Exception {
        new RegisterClient(SERVICE_INSTANCE_ID).register();
    }

}
