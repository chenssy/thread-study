package com.chenssy.client.core;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.chenssy.client.commont.constant.RegisterRquestConstant;
import com.chenssy.common.core.RecentlyChangedServiceInstance;
import com.chenssy.common.dto.*;
import com.chenssy.common.util.HttpClientResult;
import com.chenssy.common.util.HttpClientUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class HttpSender {



    /**
     * 注册
     * @param request
     * @return
     */
    public static RegisterResponse register(RegisterRequest request) {
        log.info("[注册服务] - 服务实例:{}", JSONObject.toJSONString(request));

        HttpClientResult result = HttpClientUtils.doPost(RegisterRquestConstant.REGISTER_REQUEST, (JSONObject) JSONObject.toJSON(request));
        if (200 == result.getCode()) {
            String content = result.getContent();
            return JSON.parseObject(content,RegisterResponse.class);
        } else {
            throw new RuntimeException("注册失败，返回码为:" + result.getCode());
        }
    }

    /**
     * 发送心跳
     * @param request
     * @return
     */
    public static HearbeatResponse heartbear(HearbeatRequest request) {
        log.info("[发送心跳] - 服务实例:{}",JSONObject.toJSONString(request));

        HttpClientResult result = HttpClientUtils.doPost(RegisterRquestConstant.RENEW_REQUEST, (JSONObject) JSONObject.toJSON(request));
        if (200 == result.getCode()) {
            String content = result.getContent();
            return JSON.parseObject(content,HearbeatResponse.class);
        } else {
            throw new RuntimeException("注册失败，返回码为:" + result.getCode());
        }
    }

    /**
     * 拉取注册表
     * @return
     */
    public static PullRegistryResponse pullRegister() {
        log.info("[拉取注册表].....");

        HttpClientResult result = HttpClientUtils.doGet(RegisterRquestConstant.PULL_REGISTRY_REQEUST);
        if (200 == result.getCode()) {
            return JSON.parseObject(result.getContent(),PullRegistryResponse.class);
        } else {
            throw new RuntimeException("拉取注册表失败,code:" + result.getCode());
        }
    }

    /**
     * 增量拉取注册表
     * @return
     */
    public static DeltaRegistry fetchDeltaPullRegister() {
        log.info("[增量拉取注册表].....");

        HttpClientResult result = HttpClientUtils.doGet(RegisterRquestConstant.PULL_REGISTRY_REQEUST);
        if (200 == result.getCode()) {
            return JSON.parseObject(result.getContent(),DeltaRegistry.class);
        } else {
            throw new RuntimeException("增量拉取注册表失败,code:" + result.getCode());
        }
    }


    /**
     * 服务下线
     * @param serviceName
     * @param serviceInstanceId
     */
    public static CancelResponse cancel(String serviceName,String serviceInstanceId) {
        log.info("[服务下线] - service:{},serviceInstanceId:{}",serviceName,serviceInstanceId);

        CancelRequest request = new CancelRequest(serviceName,serviceInstanceId);

        HttpClientResult result = HttpClientUtils.doDelete(RegisterRquestConstant.CANCEL_REQUEST, (JSONObject) JSONObject.toJSON(request));
        if (200 == result.getCode()) {
            return JSON.parseObject(result.getContent(),CancelResponse.class);
        } else {
            throw new RuntimeException("服务下线失败,code:" + result.getCode());
        }
    }
}
