package com.chenssy.client.core;

import com.chenssy.common.core.InstanceInfo;
import com.chenssy.common.core.Lease;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientRegistry {
    // 注册表
    private Map<String, Map<String, Lease<InstanceInfo>>> registry = new HashMap<>();
}
